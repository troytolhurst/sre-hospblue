# main.tf

provider "aws" {
  region = "eu-west-2"  # Change to your preferred AWS region
}
resource "aws_ecr_repository" "flask_app_repository" {
  name = "hosp-blue-image-registry"
}
# EC2 Instance
resource "aws_instance" "hosp_blue_routing_instance" {
  ami             = "ami-01bd90816769dc532"  # Amazon Linux 2 AMI, adjust for your region
  instance_type   = "t4g.micro"  # Change to the desired instance type
  key_name        = "georges-test-key-pair"  # Replace with your EC2 key pair name
  iam_instance_profile = aws_iam_instance_profile.ec2_instance_profile.name

  user_data = <<-EOF
              #!/bin/bash
              sudo yum install -y docker
              sudo yum install -y awscli
              export repository_url=${aws_ecr_repository.flask_app_repository.repository_url}
              service docker start
              usermod -aG docker ec2-user
              aws ecr get-login-password --region 'eu-west-2' | docker login --username AWS --password-stdin ${aws_ecr_repository.flask_app_repository.repository_url}
              # Pull and run the Flask Docker image from ECR
              docker pull ${aws_ecr_repository.flask_app_repository.repository_url}:latest
              docker run -d -p 5000:5000 --name flask-app-container ${aws_ecr_repository.flask_app_repository.repository_url}:latest
              EOF

  tags = {
    Name = "Hosp-Blue-Routing-Instance"
  }
}

resource "aws_db_instance" "example_rds" {
  identifier            = "bluehospdatabase"
  allocated_storage     = 20
  storage_type          = "gp2"
  engine                = "postgres"  # Change the engine to "postgres"
  engine_version        = "12"        # Specify the desired PostgreSQL version
  instance_class        = "db.t3.micro"
  db_name               = "vetlogs"
  username              = var.DB_USERNAME
  password              = var.DB_PASSWORD
  port                  = 5432          # PostgreSQL default port
  publicly_accessible   = true

  tags = {
    Name = "BlueHosp-Cache-Database"
  }
}

output "rds_endpoint" {
  value = aws_db_instance.example_rds.endpoint
}
# Output the public IP address of the EC2 instance
output "public_ip" {
  value = aws_instance.hosp_blue_routing_instance.public_ip
}
output "ecr_repo_url"{
    value = aws_ecr_repository.flask_app_repository.repository_url
}


resource "aws_iam_role" "ec2_instance_role" {
  name = "ec2-instance-role"
  
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecr_policy_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.ec2_instance_role.name
}
resource "aws_iam_instance_profile" "ec2_instance_profile" {
  name = "ec2-instance-profile"

  role = aws_iam_role.ec2_instance_role.name
}