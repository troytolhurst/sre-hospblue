import subprocess
import sys
import os
from retrying import retry
from flask import Flask, request, jsonify, session, redirect, url_for, render_template
import logging
import json
import psycopg2
import base64
from psycopg2 import sql
import secrets
app = Flask(__name__)
# Create a logger
app.secret_key = secrets.token_hex()
BASE_URL = 'ec2-3-8-192-136.eu-west-2.compute.amazonaws.com'
DB_USERNAME = os.environ['DB_USERNAME']
DB_PASSWORD = os.environ['DB_PASSWORD']
HOST_URL = 'bluehospdatabase.cvruukypsgyb.eu-west-2.rds.amazonaws.com'
connection = psycopg2.connect(
        host=HOST_URL,
        user=DB_USERNAME,
        password=DB_PASSWORD,
        database='vetlogs'
    )
logger = logging.getLogger(__name__)
# Configure Flask app to print logs to stdout
app.logger.addHandler(logging.StreamHandler(sys.stdout))
app.logger.setLevel(logging.INFO)
file_handler = logging.FileHandler('logfile.log')
file_handler.setLevel(logging.DEBUG)  # Set the desired logging level for the file handler
app.logger.addHandler(file_handler)
# Create a file handler and configure it to write logs to a file
process_request_logger = logging.getLogger('process_request')
process_request_logger.addHandler(file_handler)  # Add the same file handler
process_request_logger.setLevel(logging.DEBUG)
def create_table():
    try:
        with connection.cursor() as cursor:  
            cursor.execute(
                '''
                CREATE TABLE IF NOT EXISTS get_requests (
                    id SERIAL PRIMARY KEY,
                    url VARCHAR(255) NOT NULL,
                    response TEXT,
                    fetch_count INT DEFAULT 30
                );
                CREATE TABLE IF NOT EXISTS valid_users (
                    id SERIAL PRIMARY KEY,
                    staff_id INT,
                    Auth_token TEXT
                );
                '''
            )
            connection.commit()
    except Exception as e:
        connection.rollback()
        logger.error("Error creating table:", e)
# Call the function to create the table when the application starts
create_table()
# Retry decorator
def check_retry(status_code=500):
    return status_code == 500
@retry(
    stop_max_attempt_number=4,
    wait_exponential_multiplier=30,
    retry_on_result=lambda result: check_retry(result[1]) if result else True
)
def make_curl_request(url, auth_pass, data=None):
    # Make a cURL request using subprocess
    if data:
        data = json.dumps(data)
        curl_options = [
            '-XPOST',
            '-i',
            '-H', f'Authorization: {auth_pass}',
            '-H', 'Content-type: application/json',
            '-d', f'{data}'
        ]
    else:
        curl_options = [
            '-i',  # Include headers in the output
            '-H', f'Authorization: {auth_pass}'
        ]
    
    result = subprocess.run(['curl', *curl_options, url], capture_output=True, text=True)
    
    if result.returncode == 0:
        body_start_index = result.stdout.find('\n\n')
        headers = result.stdout[:body_start_index]
        body = result.stdout[body_start_index:]
        status_line = headers.split('\r\n')[0]
        status_code = int(status_line.split(' ')[1])
        if status_code == 500:
            logger.warning("Received 500 error from the server.")
            return None, status_code  # Retry only on 500 errors
        else:
            return body, status_code
    else:
        return None, None  # Retry only on non-successful exit codes
@app.route('/login', methods=['GET', 'POST']) 
def login():
    if request.method == 'POST':
        process_request_logger.debug('[DEBUG ENTERED POST METHOD]')
        username = request.form.get('username')
        password = request.form.get('password')
        auth_data = f'{username}:{password}'
        auth_data = auth_data.encode('utf-8')
        auth_pass = 'Basic ' + str(base64.b64encode(auth_data).decode())
        session['logged_in'] = validate_user(auth_pass)
        process_request_logger.debug('[DEBUG SESSION STATE] Session logged_in: ' + str(session['logged_in']))
        if session['logged_in']:
            process_request_logger.info(f'[Login Form Success]')
            process_request_logger.info(f'[Login Form URL] ' + session["url_to_process"])
            return redirect(url_for('process_request'))
        return render_template('login.html', errors='Username or password not recognised.')
        
    else:
        return render_template('login.html')
def validate_user(auth_pass):
    with connection.cursor() as cursor:
        cursor.execute(
            '''
            SELECT staff_id FROM valid_users WHERE Auth_token =%s
            ''',[auth_pass]
        )
        validity = cursor.fetchone()
        if validity:
            session['staff_id'] = validity[0]
            session['Authorization'] = auth_pass
        else:
            response, status_code = make_curl_request(f'{BASE_URL}/staffs/me',auth_pass)
            if status_code == 200:
                response = json.loads(response)
                session['Authorization'] = auth_pass
                session['staff_id'] = response['id']
                cursor.execute(
                    '''
                    INSERT INTO valid_users (staff_id,Auth_token) VALUES (%s,%s)
                    ''',[response['id'],auth_pass]
                )
                connection.commit()
    
        
    

    if session.get('staff_id'):  
        try:
            process_request_logger.debug('[DEBUG REPORT VALIDATE USER] Validity:' + str(validity))
            process_request_logger.debug('[DEBUG REPORT VALIDATE USER] Staff Id:' + str(session.get('staff_id')))   
        except:
            process_request_logger.debug('[DEBUG REPORT VALIDATE USER] Validity: User Added to database' )
            process_request_logger.debug('[DEBUG REPORT VALIDATE USER] Staff Id:' + str(session.get('staff_id')))   
        return True
    else:
        process_request_logger.debug('[DEBUG REPORT VALIDATE USER] Invalid user details passed.')   

        return False
def validate_route(url):
    valid_routes = ['hospitals', 'staffs', 'patients', 'notes']
    if any(route in url for route in valid_routes) or url == '':
        return True
@app.route('/', methods=['GET'], defaults={'url_to_process': ''})
@app.route('/<path:url_to_process>', methods=['GET'])
def process_request(url_to_process):
    user_agent = request.headers.get('User-Agent')
    if validate_route(url_to_process):
        pass
    else:
        return '404 Page Not Found',404
    try:
        query_params = request.args.to_dict()
        url_to_process = f"ec2-3-8-192-136.eu-west-2.compute.amazonaws.com/{url_to_process}?{'&'.join(f'{key}={value}' for key, value in query_params.items())}"
        if 'curl' not in user_agent and  'node' not in user_agent:
            if not session.get('logged_in'):
                session['url_to_process'] = url_to_process
                process_request_logger.info(f'[BROWSER URL PARSED] ' + session['url_to_process'])
                return redirect(url_for('login'))
            
            auth_pass = session['Authorization']
            url_to_process = session['url_to_process']
        else:
            auth_pass = request.headers.get('Authorization')
            
        process_request_logger.info(f'[Info GET Response Data:] {url_to_process}')
        
        
        if url_to_process:
            if validate_user(auth_pass):
                with connection.cursor() as cursor:  # split up contents. One thing 
                    cursor.execute(
                        '''
                        SELECT response, fetch_count FROM get_requests WHERE url=%s
                        ''',[url_to_process.strip()]
                    )
                    response_text = cursor.fetchone()
                    
                    if response_text:   # EVERY IF IN THIS BLOCK NEEDS A RETURN
                        response, fetch_count = response_text # RETUR
                        if fetch_count > 0:
                            fetch_count -= 1
                            cursor.execute(
                                '''
                                UPDATE get_requests SET fetch_count=%s WHERE url=%s
                                ''', [fetch_count, url_to_process.strip()]
                            )
                            connection.commit() # NEEDS RETURN+RESPONSE TEXT, RETURN CODE
                            
                            json_response =json.loads(response.replace('\n',''))
                            print(response.replace('\n',''))
                            print(json_response)
                            session.clear()
                            return json_response,200
                        else:
                            # Reset fetch count and make cURL request to update information
                            cursor.execute(
                                '''
                                UPDATE get_requests SET fetch_count=30 WHERE url=%s
                                ''', [url_to_process.strip()]
                            )
                            connection.commit()
                            session.clear()
                            return response, 200
                response, status_code = make_curl_request(url_to_process, auth_pass)
                
                if response:
                    with connection.cursor() as cursor:
                        cursor.execute(
                            '''
                            INSERT INTO get_requests (url, response) VALUES (%s,%s)
                            ''', [url_to_process,str(json.dumps(response))]
                        )
                        connection.commit()
                    session.clear()
                    return response, status_code
                else:
                    session.clear()
                    return jsonify({'status': 'failed', 'message': 'Request failed'}), 500
            else:
                session.clear()
                return 'HTTP Basic: Access denied.',401
        else:
            session.clear()
            return jsonify({'status': 'failed', 'message': 'URL not provided in the request'}), 400
    except Exception as e:
        session.clear()
        return jsonify({'status': 'failed', 'message': str(e)}), 500
@app.route('/<path:url_to_process>', methods=['POST'])
def process_post_request(url_to_process):
    if validate_route(url_to_process):
        pass
    else:
        return '404 Page Not Found',404
    try:
        url_to_process = "ec2-3-8-192-136.eu-west-2.compute.amazonaws.com/" + url_to_process
        auth_pass = request.headers.get('Authorization')
        if request.headers['Content-Type'] == 'application/json':
            try:
                incoming_data = request.get_json()
            except:
                incoming_data = None
        else:
            try:
                incoming_data = request.get_data(as_text=True)
            except:
                incoming_data = None
        process_request_logger.info(f'[Info POST Response Data:] {url_to_process}')
        if url_to_process:
            if incoming_data == None:
                return '400 - Your request is invalid',400
            response, status_code = make_curl_request(url_to_process, auth_pass, incoming_data)
            if response:
                if status_code == 201 or 'screen' in url_to_process:
                    route_path = str(url_to_process.split('/')[1])
                    with connection.cursor() as cursor:
                        cursor.execute('''
                            UPDATE get_requests set fetch_count=0 WHERE position(%s in url)>0
                            '''
                        ,[route_path])
                        connection.commit()
                return response, status_code
            else:
                return jsonify({'status': 'failed', 'message': 'Request failed'}), 500
        else:
            return jsonify({'status': 'failed', 'message': 'URL not provided in the request'}), 400
    except Exception as e:
        return jsonify({'status': 'failed', 'message': str(e)}), 500
if __name__ == '__main__':
    app.run(debug=True)









