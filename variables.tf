variable "DB_USERNAME" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
  default = "USER_NAME"
}
variable "DB_PASSWORD" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
}